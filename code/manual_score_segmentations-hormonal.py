#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 10:44:20 2021

@author: henrikahl
"""

import os
import re
import pandas as pd
import pyvista as pv
from imgmisc import listdir
from imgmisc import mkdir
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains'
OUTPUT_DIR = f'{PROJECT_DIR}/data/hormomal_segmentation_scores'
mkdir(OUTPUT_DIR)

files = listdir(f'{INPUT_DIR}')
files = sorted(files, key=lambda x: bname(x).split('-')[-3])

genotype = '-abcb1-'

scores = []
settings = []
for ff in files:
    if genotype not in ff:
        continue
    
    mesh = pv.read(ff)
    p = pv.Plotter(title=bname(ff))
    p.add_mesh(mesh, scalars='domains', cmap='glasbey', interpolate_before_map=False, categories=True)
    p.add_key_event('1', lambda: scores.append((bname(ff), 1)))
    p.add_key_event('2', lambda: scores.append((bname(ff), 2)))
    p.add_key_event('3', lambda: scores.append((bname(ff), 3)))
    p.add_key_event('4', lambda: scores.append((bname(ff), 4)))
    p.add_key_event('5', lambda: scores.append((bname(ff), 5)))
    p.add_key_event('s', lambda: scores.append((bname(ff), 's')))
    p.view_yz()
    p.show()
    p.deep_clean()

# Create dataframe and save to file    
df = pd.DataFrame(data={'file':[ff[0] for ff in scores], 'score':[ff[1] for ff in scores]})
df['dataset'], df['reporter'], df['genotype'], df['plant'] = zip(*[re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)', xx)[0] for xx in df['file']])
df['dataset'], df['plant'] = df['dataset'].astype(int), df['plant'].astype(int)

df.to_csv(f'{OUTPUT_DIR}/segmentation_scores.csv', sep='\t', index=False)

# Print stats of interest
print(df.loc[df['score'].isin([3, 4, 5])].groupby(['genotype', 'score']).size())
print(df.loc[df['score'].isin([3, 4, 5])].groupby(['genotype']).size())
print(df.loc[df['score'].isin([4, 5])].groupby(['genotype']).size())
print(df.loc[df['score'].isin(['s'])].groupby(['genotype']).size())
