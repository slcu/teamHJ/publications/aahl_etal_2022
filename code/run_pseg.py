import os
import torch
import argparse
import numpy as np
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import get_resolution
from tempfile import NamedTemporaryFile

def bname(x): return os.path.basename(os.path.splitext(x)[0])

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')
parser.add_argument('--factor', type=float,
                    help='Scaling factor', default=None, nargs=3)

# Optional argument
parser.add_argument('--device', type=str,
                    help='cuda or cpu', default='cuda')
parser.add_argument('--model', type=str,
                    help='model', default='confocal_PNAS_3d')
parser.add_argument('--mirror_padding', type=int,
                    nargs=3, default=[16, 32, 32])
parser.add_argument('--patch', type=int, nargs=3, default=[64, 128, 128])
parser.add_argument('--stride', type=int, nargs=3, default=[32, 64, 64])
parser.add_argument('--pre_sigma', type=float, default=0)
parser.add_argument('--seg_alg', type=str, default='MultiCut',
                    choices=['MultiCut', 'GASP', 'DtWatershed', 'MutexWS', 'SimpleITK'])
parser.add_argument('--seg_beta', type=float, default=.6)
parser.add_argument('--seg_prob_thres', type=float, default=.5)
parser.add_argument('--seg_sigma', type=float, default=2)
parser.add_argument('--seg_minsize', type=int, default=50)
parser.add_argument('--seg_sup_minsize', type=int, default=50)
parser.add_argument('--num_workers', type=int, default=6)
parser.add_argument('--resolution', type=float, nargs=3, default=None)

parser.add_argument('--no_preprocess', action='store_false')
parser.add_argument('--no_segment', action='store_false')
parser.add_argument('--no_cnn_predict', action='store_false')

parser.add_argument('--seg_tif', action='store_true')
parser.add_argument('--cnn_tif', action='store_true')
parser.add_argument('--update_model', action='store_true')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--pseg_bin', type=str,
                    help='PlantSeg binary path', default=f'{os.path.expanduser("~")}/.local/anaconda3/envs/plant-seg/bin')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

def config(path, factor,
           model='confocal_unet_bce_dice_ds3x',
           device='cuda',
           mirror_padding=[16, 32, 32],
           patch=[64, 128, 128], stride=[32, 64, 64],
           pre_sigma=1,
           seg_alg='MultiCut',
           seg_beta=.6,
           seg_prob_thres=.5,
           seg_sigma=2,
           seg_minsize=50,
           seg_sup_minsize=50,
           cnn_tif=False,
           seg_tif=True,
           preprocess=True,
           cnn_prediction=True,
           segment=True,
           num_workers=6,
           update_model=False,
           save_dir='PreProcessing'):
    config_str = f'''path: {path}

preprocessing:
  # enable/disable preprocessing
  state: {True if preprocess else False}
  # create a new sub folder where all results will be stored
  save_directory: "{save_dir}"
  # rescaling the volume is essential for the generalization of the networks. The rescaling factor can be computed as the resolution
  # of the volume at hand divided by the resolution of the dataset used in training. Be careful, if the difference is too large check for a different model.
  factor: {list(factor)}
  # the order of the spline interpolation
  order: 2
  # crop volume
  crop_volume: "[:,:,:]"
  # optional: perform Gaussian smoothing or median filtering on the input.
  filter:
    # enable/disable filtering
    state: {False if pre_sigma == 0 else True}
    # Accepted values: 'gaussian'/'median'
    type: gaussian
    # sigma (gaussian) or disc radius (median)
    filter_param: {pre_sigma}

cnn_prediction:
  # enable/disable UNet prediction
  state: {True if cnn_prediction else False}
  # Trained model name, more info on available models and custom models in the README
  model_name: "{model}"
  # If a CUDA capable gpu is available and corrected setup use "cuda", if not you can use "cpu" for cpu only inference (slower)
  device: "{device}"
  # (int or tuple) mirror pad the input stack in each axis for best prediction performance
  mirror_padding: {list(mirror_padding)}
  # how many subprocesses to use for data loading
  num_workers: {num_workers}
  # patch size given to the network (adapt to fit in your GPU mem)
  patch: {list(patch)}
  # stride between patches (make sure the the patches overlap in order to get smoother prediction maps)
  stride: {list(stride)}
  # "best" refers to best performing on the val set (recommended), alternatively "last" refers to the last version before interruption
  version: best
  # If "True" forces downloading networks from the online repos
  model_update: {update_model}

cnn_postprocessing:
  # enable/disable cnn post processing
  state: {True if cnn_tif else False}
  # if True convert to result to tiff
  tiff: {True if cnn_tif else False}
  # rescaling factor
  factor: [1, 1, 1]
  # spline order for rescaling
  order: 2


segmentation:
  # enable/disable segmentation
  state: {True if segment else False}
  # Name of the algorithm to use for inferences. Options: MultiCut, MutexWS, GASP, DtWatershed
  name: "{seg_alg}"
  # Segmentation specific parameters here
  # balance under-/over-segmentation; 0 - aim for undersegmentation, 1 - aim for oversegmentation. (Not active for DtWatershed)
  beta: {seg_beta}
  # directory where to save the results
  save_directory: "{seg_alg}"
  # enable/disable watershed
  run_ws: True
  # use 2D instead of 3D watershed
  ws_2D: False
  # probability maps threshold
  ws_threshold: {seg_prob_thres}
  # set the minimum superpixels size
  ws_minsize: {seg_sup_minsize}
  # sigma for the gaussian smoothing of the distance transform
  ws_sigma: {seg_sigma}
  # sigma for the gaussian smoothing of boundary
  ws_w_sigma: 0
  # set the minimum segment size in the final segmentation. (Not active for DtWatershed)
  post_minsize: {seg_minsize}

segmentation_postprocessing:
  # enable/disable segmentation post processing
  state: {True if seg_tif else False}
  # if True convert to result to tiff
  tiff: {True if seg_tif and seg_alg is not None else False}
  # rescaling factor
  factor: {list(1./np.array(factor))}
  # spline order for rescaling (keep 0 for segmentation post processing
  order: 0
  save_raw: False
'''
    return config_str

# confocal_unet_bce_dice_ds1x - a variant of 3D U-Net trained on confocal images of Arabidopsis ovules on original resolution, voxel size: (0.235x0.075x0.075 µm^3) (ZYX) with BCEDiceLoss
# confocal_unet_bce_dice_ds2x - a variant of 3D U-Net trained on confocal images of Arabidopsis ovules on 1/2 resolution, voxel size: (0.235x0.150x0.150 µm^3) (ZYX) with BCEDiceLoss
# confocal_unet_bce_dice_ds3x - a variant of 3D U-Net trained on confocal images of Arabidopsis ovules on 1/3 resolution, voxel size: (0.235x0.225x0.225 µm^3) (ZYX) with BCEDiceLoss
# lightsheet_unet_bce_dice_ds1x - a variant of 3D U-Net trained on light-sheet images of Arabidopsis lateral root on original resolution, voxel size: (0.25x0.1625x0.1625 µm^3) (ZYX) with BCEDiceLoss
# lightsheet_unet_bce_dice_ds2x - a variant of 3D U-Net trained on light-sheet images of Arabidopsis lateral root on 1/2 resolution, voxel size: (0.25x0.325x0.325 µm^3) (ZYX) with BCEDiceLoss
# lightsheet_unet_bce_dice_ds3x - a variant of 3D U-Net trained on light-sheet images of Arabidopsis lateral root on 1/3 resolution, voxel size: (0.25x0.4875x0.4875 µm^3) (ZYX) with BCEDiceLoss
# confocal_2D_unet_bce_dice_ds1x - a variant of 2D U-Net trained on confocal images of Arabidopsis ovules. Training the 2D U-Net is done on the Z-slices (pixel size: 0.075x0.075 µm^3) with BCEDiceLoss
# confocal_2D_unet_bce_dice_ds2x - a variant of 2D U-Net trained on confocal images of Arabidopsis ovules. Training the 2D U-Net is done on the Z-slices (1/2 resolution, pixel size: 0.150x0.150 µm^3) with BCEDiceLoss
# confocal_2D_unet_bce_dice_ds3x - a variant of 2D U-Net trained on confocal images of Arabidopsis ovules. Training the 2D U-Net is done on the Z-slices (1/3 resolution, pixel size: 0.225x0.225 µm^3) with BCEDiceLoss
# lightsheet_unet_bce_dice_nuclei_ds1x - a variant of 3D U-Net trained on light-sheet images Arabidopsis lateral root nuclei on original resolution, voxel size: (0.25x0.1625x0.1625 µm^3) (ZYX) with BCEDiceLoss

if args.model == 'confocal_PNAS_3d':
    ref_res = np.array([0.25, 0.25, 0.25])
elif args.model == 'confocal_unet_bce_dice_ds1x':
    ref_res = np.array([0.235, 0.075, 0.075])
elif args.model == 'confocal_unet_bce_dice_ds2x':
    ref_res = np.array([0.235, 0.150, 0.150])
elif args.model == 'confocal_unet_bce_dice_ds3x':
    ref_res = np.array([0.235, 0.225, 0.225])
elif args.model == 'lightsheet_unet_bce_dice_ds1x':
    ref_res = np.array([0.25, 0.1625, 0.1625])
elif args.model == 'lightsheet_unet_bce_dice_ds2x':
    ref_res = np.array([0.25, 0.325, 0.325])
elif args.model == 'lightsheet_unet_bce_dice_ds3x':
    ref_res = np.array([0.25, 0.4875, 0.4875])
elif args.model == 'confocal_unet_bce_dice_nuclei_stain_ds1x':
    ref_res = np.array([0.35, 0.1, 0.1])

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff',
                                                               '.lsm', '.czi'])
else:
    files = args.input
mkdir(args.output)
# print(files)

print(files)
#for fname in files:
def run(fname):
    if args.skip_done:
        splitstr = '_predictions' if args.no_segment else '_predictions_'
        done_files = listdir(args.output, recursive=True, sorting='natural')
        done_files = np.array([bname(ff).split(splitstr)[0] for ff in done_files if splitstr in bname(
            ff) and os.path.getsize(ff) > 1000])  # 1000 = arbitrary minsize for file being finished
        if any(done_files == bname(fname)):
            if args.verbose:
                print(f'Skipping {fname} - done')
            return
    if args.verbose:
        print(f'Now running {fname}')

    if args.resolution is None and args.factor is None:
        resolution = np.asarray(get_resolution(fname))
        if resolution[0] < 1e-3:
            resolution = np.array(resolution) * 1e6
    else:
        resolution = args.resolution
    print(f'Resolution: {resolution}')

    factor = resolution / ref_res if args.factor is None else args.factor
    cfg = config(fname,
                 model=args.model,
                 factor=factor,

                 preprocess=args.no_preprocess,
                 pre_sigma=args.pre_sigma,

                 cnn_prediction=args.no_cnn_predict,
                 patch=args.patch,
                 stride=args.stride,

                 segment=args.no_segment,
                 seg_alg=args.seg_alg,
                 seg_tif=args.seg_tif,
                 seg_beta=args.seg_beta,
                 seg_prob_thres=args.seg_prob_thres,
                 seg_sigma=args.seg_sigma,
                 seg_minsize=args.seg_minsize,
                 seg_sup_minsize=args.seg_sup_minsize,

                 num_workers=args.num_workers,
                 update_model=args.update_model,
                 save_dir=args.output)

    tmpfile = NamedTemporaryFile('w')
    tmpfile.write(cfg)
    tmpfile.flush()

    torch.cuda.empty_cache()
    try:
        os.system(f'{args.pseg_bin} --config {tmpfile.name}')
        # rm_files = listdir(OUTPUT_DIR, recursive=True, include=['.h5', '.yaml'], sorting='natural')
        # rm_files = [ff for ff in rm_files if 'predictions.h5' not in ff]
        # for ff in rm_files:
        #     os.remove(ff)
    except:
        return
    
from multiprocessing import Pool
p = Pool(args.parallel_workers)
output = p.map(run, files)
p.close()
p.join()
