#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 10:44:20 2021

@author: henrikahl
"""

import os
import re
import pandas as pd
import pyvista as pv
from imgmisc import listdir
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_domains'
OUTPUT_DIR = f'{PROJECT_DIR}/data/redo_segmentations'

files = listdir(INPUT_DIR)
files = sorted(files, key=lambda x: bname(x).split('-')[-3])
subset = '-Col0-'
files = [ff for ff in files if subset in ff]

# if 'd' is pressed, add the contour to a list of meshes which should be fixed
to_redo = []
for ff in files:
    to_add = []
    dataset, reporters, genotype, plant = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)', bname(ff))[0]
    dataset, plant = int(dataset), int(plant)

    while len(to_add) != 1:
        mesh = pv.read(ff)
        p = pv.Plotter(title=bname(ff))
        p.add_mesh(mesh, scalars='domains', cmap='glasbey', interpolate_before_map=False, categories=True)
        p.add_key_event('d', lambda: to_add.append((bname(ff), 1))) 
        p.view_yz()
        p.show()
        p.deep_clean()
        
        if len(to_add) == 0:
            to_add.append((bname(ff), 0))
    to_redo.append(to_add)

# df = pd.read_csv(f'{OUTPUT_DIR}/redo_contours.csv', sep='\t')
df = pd.DataFrame({'file':[dd[0][0] for dd in to_redo], 'redo':[dd[0][1] for dd in to_redo]})
df['dataset'], df['reporter'], df['genotype'], df['plant'] = zip(*[re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)', xx)[0] for xx in df['file']])
df['dataset'], df['plant'] = df['dataset'].astype(int), df['plant'].astype(int)
df.to_csv(f'{OUTPUT_DIR}/redo_segmentations{subset}.csv', sep='\t', index=False)