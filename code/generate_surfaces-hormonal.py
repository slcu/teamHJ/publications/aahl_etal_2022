#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 11:53:44 2020

@author: henrik
"""

import os
import sys
import numpy as np
import tifffile as tiff
from imgmisc import listdir
from imgmisc import mkdir
from imgmisc import get_resolution
from multiprocessing import Pool
from phenotastic.mesh import remesh    
from phenotastic.mesh import create_mesh
from phenotastic.mesh import repair_small
from phenotastic.mesh import remove_tongues
from phenotastic.mesh import smooth_boundary

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_contours'
OUTPUT_DIR = f'{PROJECT_DIR}/data/hormonal_surfaces'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, sorting='natural', include='.tif')

# algorithm parameters
hole_repair_threshold = 100
downscaling = 0.025
smooth_iter = 200
smooth_relax = 0.01
upscaling = 2
tongues_ratio = 3
tongues_radius = 10

fname = files[0]
def run(fname):
    bname = f'{os.path.splitext(os.path.basename(fname))[0]}_mesh.vtk'
    if any([f'{OUTPUT_DIR}/{bname}' in ff for ff in listdir(OUTPUT_DIR, include='.vtk')]):
        return
    
    contour = tiff.imread(fname)
    resolution = get_resolution(fname)
    contour = np.pad(contour, 1) # just to make it so all tissue parts touching the image borders don't end up with big gaps
    contour = contour[1:]
    
    mesh = create_mesh(contour, resolution)
    mesh = mesh.extract_largest()
    mesh = mesh.clean()
    mesh = repair_small(mesh, hole_repair_threshold)
    mesh = remesh(mesh, int(mesh.n_points * downscaling), sub=0)

    mesh = remove_tongues(mesh, radius=tongues_radius, threshold=tongues_ratio, hole_edges=hole_repair_threshold)
    mesh = mesh.extract_largest().clean()
    mesh = repair_small(mesh, hole_repair_threshold)
    mesh = mesh.smooth(smooth_iter, smooth_relax)
    mesh = remesh(mesh, int(upscaling * mesh.n_points))
    mesh = smooth_boundary(mesh, smooth_iter, smooth_relax)

    mesh.save(f'{OUTPUT_DIR}/{bname}')

n_cores = int(sys.argv[1]) if len(sys.argv) > 1 else 4
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()
