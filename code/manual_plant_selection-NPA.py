#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import re
import numpy as np
import pandas as pd
import pyvista as pv
import tifffile as tiff
from imgmisc import listdir
from imgmisc import create_mesh
from imgmisc import get_resolution
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data/NPA_segmented_refined'
OUTPUT_DIR = f'{PROJECT_DIR}/data'

seg_files = listdir(INPUT_DIR, include='-refined', sorting='natural')
df = pd.DataFrame(columns=['dataset', 'genotype', 'reporters', 'plant', 't', 'good'])

iter_ = 0
for iter_ in range(len(seg_files)):
    print(f'Running {bname(seg_files[iter_])}. {iter_ / len(seg_files)} done...')
    dataset, genotype, reporters, plant, time = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)-(\d+)h', bname(seg_files[iter_]))[0]
    dataset, plant, time = int(dataset), int(plant), int(time)

    resolution = get_resolution(seg_files[iter_])
    seg_data = np.pad(tiff.imread(seg_files[iter_]) > 0, 1)
    seg_data = seg_data[1:]
    mesh = create_mesh(seg_data, resolution=resolution)
    mesh = mesh.extract_largest()
    mesh = mesh.clean()

    good = []
    while len(good) != 1:
        good = []
        p = pv.Plotter(title=bname(seg_files[iter_]))
        p.set_background('white')
        p.add_mesh(mesh, color='red')
        p.view_yz()
        p.add_key_event('g', lambda: good.append(True))
        p.add_key_event('b', lambda: good.append(False))
        p.show()
        p.deep_clean()

    df = df.append({'dataset': dataset,
                    'genotype': genotype,
                    'reporters': reporters,
                    'plant': plant,
                    't': time,
                    'good': True if good[0] else False
                    }, ignore_index=True)
df.to_csv(f'{OUTPUT_DIR}/NPA_manual_selection.csv', index=False, sep='\t')

