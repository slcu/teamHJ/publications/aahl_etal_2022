#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 19:12:52 2021

@author: henrikahl
"""

import os
import numpy as np
import tifffile as tiff
import matplotlib
import matplotlib.pyplot as plt
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8

bname = lambda x: os.path.basename(os.path.splitext(x)[0])
cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ['black', "red"])

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/data/hormonal_raw'
OUTPUT_DIR = f'{PROJECT_DIR}/figures/projections'
mkdir(OUTPUT_DIR)

files = listdir(INPUT_DIR, include ='.tif')

for fname in files:
    data = tiff.imread(fname)
    oname = bname(fname)
    
    sum_data = data.sum(0)
    sum_data = to_uint8(sum_data, normalize=True)
    plt.imsave(f'{OUTPUT_DIR}/{oname}-top_sum.png', sum_data, cmap=cmap, vmin=0, vmax=np.quantile(sum_data, .99))
        
    data_1 = data[::-1, :, data.shape[2] // 2]    
    data_1 = to_uint8(data_1, normalize=True)
    plt.imsave(f'{OUTPUT_DIR}/{oname}-side_1.png', data_1, cmap=cmap, vmin=0, vmax=np.quantile(data_1, .99))

    data_2 = data[::-1, data.shape[1] // 2, :]    
    data_2 = to_uint8(data_2, normalize=True)
    plt.imsave(f'{OUTPUT_DIR}/{oname}-side_2.png', data_2, cmap=cmap, vmin=0, vmax=np.quantile(data_2, .99))