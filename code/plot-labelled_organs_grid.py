#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 09:57:49 2020

@author: henrik
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image 
from imgmisc import mkdir
from imgmisc import listdir

# Input parameters
PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
INPUT_DIR = f'{PROJECT_DIR}/figures/single_labelled_organs'
OUTPUT_DIR = f'{PROJECT_DIR}/figures'
files = listdir(f'{INPUT_DIR}', include='.png', sorting='natural')

result_figsize_resolution = 40 # 1 = 100px
mkdir(OUTPUT_DIR)

grid_width = 17 #math.ceil(math.sqrt(len(plant_singles_files)))
grid_height = int(np.round(len(files)/grid_width + .4999))
print(len(files) / grid_width)
fig, axes = plt.subplots(int(np.round(len(files)/grid_width + .4999)), grid_width,  figsize=(40 * (297 / 210), 40))

files = sorted(files, key=lambda x: int(x.split('domain_')[1].split('.png')[0]))

current_row = -1
current_col = -1
for idx, fname in enumerate(files):
    if idx % grid_width == 0:
        current_row += 1
    current_col += 1
    current_col = current_col % grid_width
    
    img = Image.open(files[idx])
    axes[current_row, current_col].imshow(img)
    
    print(f'{idx + 1}/{len(files)}')

for xx in range(axes.shape[0]):
    for yy in range(axes.shape[1]):
        axes[xx, yy].axis('off')

plt.subplots_adjust(left=0.0, right=1.0, bottom=0.0, top=1.0, wspace=0, hspace=0)
plt.savefig(f'{OUTPUT_DIR}/flowers-labelled_organs.png')
plt.close()
