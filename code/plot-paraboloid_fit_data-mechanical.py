#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import mannwhitneyu
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

sns.set(font_scale=3)
sns.set_style('ticks')
sns.set_palette('tab20')

PROJECT_DIR = f'{os.path.expanduser("~")}/projects/aahl_etal_2022'
OUTPUT_DIR = f'{PROJECT_DIR}/figures'
df = pd.read_csv(f'{PROJECT_DIR}/data/paraboloid_fit_data-mechanical.csv', sep='\t')

df['Gaussian curvature'] = df['p0'] * df['p1'] 
df['Mean curvature'] = df[['p0', 'p1']].abs().mean(1)
df['Maximal curvature'] = df[['p0', 'p1']].abs().max(1)
df['Minimal curvature'] = df[['p0', 'p1']].abs().min(1)
df['Principal curvature ratio'] = df[['p0', 'p1']].abs().max(1) / df[['p0', 'p1']].abs().min(1)
df['gaussian_curvature_mean'] = df.groupby(['dataset', 'genotype']).transform('mean')['Gaussian curvature']

df = df.loc[~df['genotype'].isin(['det3', 'cu4', 'gabi', 'bag', 'cob6'])]

df.loc[df['genotype'] == 'csi1_3', 'genotype'] = 'csi1'
df.loc[df['genotype'] == 'act2_5', 'genotype'] = 'act2'
df.loc[df['genotype'] == 'prc1_1', 'genotype'] = 'prc1'

df.loc[df['genotype'] == 'prc1', 'genotype'] = 'cesa6^{prc1}'
df.loc[df['genotype'] == 'any1', 'genotype'] = 'cesa1^{any1}'
df.loc[df['genotype'] == 'eli1', 'genotype'] = 'cesa3^{eli1}'
df.loc[df['genotype'] == 'je5', 'genotype'] = 'cesa3^{je5}'

df.loc[df['genotype'] == 'xxt1xxt2', 'genotype'] = 'xxt1,2'
df.loc[df['genotype'] == 'xxt1xxt2xxt5', 'genotype'] = 'xxt1,2,5'
df.loc[df['genotype'] == 'cmu1cmu2', 'genotype'] = 'cmu1,2'

df.loc[df['genotype'] == 'Col0', 'genotype'] = 'Col-0'

df = df.loc[df['genotype'].isin(['Col-0', 'xxt1,2', 'xxt1,2,5', 'cesa1^{any1}', 'cesa6^{prc1}', 'cesa3^{eli1}', 'cesa3^{je5}'])]

for col in df['genotype'].unique()[df['genotype'].unique() != 'Col-0']:
    df.loc[df['genotype'] == col, 'genotype'] = '$\it{' + f'{col}' + '}$'
df = df.loc[df['para_success'] == 1]
print(f'Number of plants: {df["genotype"].shape[0]}')

# order = df.sort_values('gaussian_curvature_mean')['genotype'].drop_duplicates().values
order = df.sort_values('genotype')['genotype'].drop_duplicates().values
order = np.append(['Col-0'], order[order != 'Col-0'])
order[-2], order[-1] = order[-1], order[-2]

col0_colors = sns.color_palette('Greys', 1)#['black'] #[matplotlib.cm.get_cmap('Greys')(idx) for idx in range(1)]
# pid_colors = sns.color_palette('summer', 1)#['silver'] #[matplotlib.cm.get_cmap('Oranges')(idx) for idx in range(1)]
cesa_colors = sns.color_palette('Reds', 4)#['pink', 'deeppink', 'hotpink'] #[matplotlib.cm.get_cmap('Reds')(range(3))[idx] for idx in range(3)]
xxt_colors = sns.color_palette('Greens', 2) #['khaki', 'gold', 'goldenrod'] #[matplotlib.cm.get_cmap('Greens')(range(2))[idx] for idx in range(2)]
# pin_colors = sns.color_palette('Blues', 7)#['lightsteelblue', 'cornflowerblue', 'skyeblue', 'deepskyblue', 'royalblue', 'blue', 'navy'] #[matplotlib.cm.get_cmap('Blues')(range(7))[idx] for idx in range(7)]
colors = np.vstack([col0_colors, cesa_colors, xxt_colors])
df['order'] = pd.Categorical(df['genotype'], order)

line_length = 1000 * df['genotype'].unique().shape[0] + 500 - 1000
### Gaussian curvature
plt.figure(figsize=(14, 8))
ax = sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'genotype', 'reporters']), x='genotype', y='Gaussian curvature', order=order, palette=colors, showfliers=False)
ax = sns.stripplot(data=df, x='genotype', y='Gaussian curvature', order=order, palette=colors, dodge=True, linewidth=1, edgecolor='black')
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.axhline(y=df.loc[df['genotype'] == 'Col-0', 'Gaussian curvature'].median(), color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)
# ax = sns.lineplot(x=np.arange(0, line_length) / 1000, y=np.ones(line_length) * df.loc[df['genotype'] == 'Col-0', 'Gaussian curvature'].median(), color='r', lw=4, linestyle='--', ax=ax)
plt.ylim(0, 2.2e-4)
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()  
plt.savefig(f'{OUTPUT_DIR}/meristems-gaussian_curvature_mechanical.svg')

### Compute pvals
pvals = []
mannwhitneyu(df.loc[df['genotype'] == 'Col-0', 'Gaussian curvature'], df.loc[df['genotype'] != 'Col-0', 'Gaussian curvature'])[1]
for gt in order[1:]:
    pvals.append((gt, mannwhitneyu(df.loc[df['genotype'] == 'Col-0', 'Gaussian curvature'], df.loc[df['genotype'] == gt, 'Gaussian curvature'])[1]))
pvals = np.array(pvals)
sns.set_style('ticks')
# matplotlib.rcParams.update({'font.size': 22})
plt.figure(figsize=(14, 8))
ax = sns.barplot(x=pvals[:,0], y=pvals[:,1].astype(float), palette=colors[1:], linewidth=4, edgecolor='black')
plt.axhline(y=0.05, color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)

ax.set_yscale("log")
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Gaussian curv.\np-value')
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/meristems-p_values_gaussian_curvatures-mechanical.svg')

# Curvature ratio
df['Pr. curv. ratio'] = df['Principal curvature ratio']
plt.figure(figsize=(14, 8))
ax = sns.boxplot(data=df, x='genotype', y='Principal curvature ratio', order=order, palette=colors, showfliers=False)
ax = sns.stripplot(data=df, x='genotype', y='Principal curvature ratio', order=order, palette=colors, dodge=True, linewidth=1, edgecolor='black')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.axhline(y=df.loc[df['genotype'] == 'Col-0', 'Principal curvature ratio'].median(), color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)
plt.xticks(rotation=45, ha='right')
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ylim(.9,1.75)
plt.ylabel('Pr. curv. ratio')
plt.xlabel('Genotype')
plt.tight_layout()  
plt.savefig(f'{OUTPUT_DIR}/meristems-principal_curvature_ratio_mechanical.svg')

### Compute pvals
pvals = []
mannwhitneyu(df.loc[df['genotype'] == 'Col-0', 'Principal curvature ratio'], df.loc[df['genotype'] != 'Col-0', 'Principal curvature ratio'])[1]
for gt in order[1:]:
    pvals.append((gt, mannwhitneyu(df.loc[df['genotype'] == 'Col-0', 'Principal curvature ratio'], df.loc[df['genotype'] == gt, 'Principal curvature ratio'])[1]))
pvals = np.array(pvals)
sns.set_style('ticks')
plt.figure(figsize=(14, 8))
ax = sns.barplot(x=pvals[:,0], y=pvals[:,1].astype(float), palette=colors[1:], linewidth=4, edgecolor='black')
plt.axhline(y=0.05, color='red', linestyle='--', xmin=0/3, xmax=3/3, linewidth=4)
ax.set_yscale("log")
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Pr. curv. ratio\np-value')
plt.xticks(rotation=45, ha='right')
plt.xlabel('Genotype')
plt.tight_layout()
plt.savefig(f'{OUTPUT_DIR}/meristems-p_values_principal_curvature_ratio-mechanical.svg')