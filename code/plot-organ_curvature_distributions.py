# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# """
# Created on Mon Oct 25 21:53:05 2021

# @author: henrikahl
# """

# import os
# import pandas as pd
# import seaborn as sns
# import matplotlib.pyplot as plt
# from imgmisc import listdir

# PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
# INPUT_DIR = f'{PROJECT_DIR}/data/curvatures'
# OUTPUT_DIR = f'{PROJECT_DIR}/figures'

# files = listdir(INPUT_DIR)

# df = pd.read_csv([ff for ff in files if 'Col0' in ff][0], sep='\t')
# df = df.drop('file', axis=1)
# odf = pd.read_csv(f'{PROJECT_DIR}/data/manual_organ_ordering/manual_organ_ordering-Col0.csv', sep='\t')
# df = df.merge(odf, left_on=['dataset', 'genotype', 'reporters', 'plant', 'domain'], right_on=['dataset', 'genotype', 'reporters', 'plant', 'organ'])
# df = df.dropna()
# bakdf = df.copy()

# df = bakdf.copy()
# df = df.loc[df['meancurv'] > -0.075]
# df = df.loc[df['meancurv'] < 0.05]
# sns.kdeplot(data=df, x='meancurv', hue='ordered_index_reversed', common_norm=False, palette='turbo', bw=0.15, lw=4)
# plt.xlabel('Mean vertex curvature')

# df = bakdf.copy()
# df = df.loc[df['gausscurv'] < .003]
# df = df.loc[df['gausscurv'] > -.002]
# sns.kdeplot(data=df, x='gausscurv', hue='ordered_index_reversed', common_norm=False, palette='turbo', bw=0.15, lw=4)
# plt.xlabel('Gaussian vertex curvature')

# df = bakdf.copy()
# df = df.loc[df['mincurv'] < .01]
# df = df.loc[df['mincurv'] > -.1]
# sns.kdeplot(data=df, x='mincurv', hue='ordered_index_reversed', common_norm=False, palette='turbo', bw=0.15, lw=4)
# plt.xlabel('Minimal vertex curvature')

# df = bakdf.copy()
# df = df.loc[df['maxcurv'] < .05]
# df = df.loc[df['mincurv'] > -.05]
# sns.kdeplot(data=df, x='maxcurv', hue='ordered_index_reversed', common_norm=False, palette='turbo', bw=0.15, lw=4)
# plt.xlabel('Maximal vertex curvature')
