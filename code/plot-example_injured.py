#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl
"""
import os
import numpy as np
import mahotas as mh
import pyvista as pv
import tifffile as tiff
import phenotastic.mesh as mp
import phenotastic.domains as boa
from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import autocrop
from imgmisc import create_mesh
from imgmisc import get_resolution
from imgmisc import binary_extract_largest
from scipy.ndimage import zoom
from scipy.ndimage import gaussian_filter
from skimage.segmentation import morphological_chan_vese
from phenotastic.mesh import remesh    
from phenotastic.mesh import fill_beneath
from phenotastic.mesh import repair_small
from phenotastic.mesh import remove_tongues
from phenotastic.mesh import smooth_boundary
from skimage.exposure import equalize_adapthist
import matplotlib
import matplotlib.pyplot as plt

bname = lambda x: os.path.basename(os.path.splitext(x)[0])

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/aahl_etal_2022')
INPUT_DIR = f'{PROJECT_DIR}/data/example_injured'
OUTPUT_DIR = f'{PROJECT_DIR}/figures'
files = listdir(INPUT_DIR, sorting='natural')
fname = files[0]

data = tiff.imread(fname)
resolution = get_resolution(fname)

iterations = 25
smoothing = 1
masking = 1
clahe_window = None
clahe_clip_limit = None
gaussian_sigma = [1, 1, 1]
gaussian_iterations = 5
target_res = .5, .5, .5
fill_slices = True
lambda1 = 1
lambda2 = 1
clip_threshold = .01
verbose = True

hole_repair_threshold = 100
downscaling = 0.025
smooth_iter = 200
smooth_relax = 0.01
upscaling = 2
tongues_ratio = 3
tongues_radius = 10

curvature_limit = 0.01

cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ['black', "red"])

# Plot the summed topprojection of the raw data
sum_data = data.sum(0)
plt.imsave(f'{PROJECT_DIR}/example_injured-0_confocal_top_sum.png', sum_data, cmap=cmap, vmin=0, vmax=np.quantile(sum_data, .99))

# Preprocess
data = autocrop(data, n=150, offset=10)
data = zoom(data, resolution / np.array(target_res), order=3)
clahe_window = (np.array(data.shape) + 4) // 8
data = equalize_adapthist(data, clahe_window, clip_threshold)

for ii in range(gaussian_iterations):
    data = gaussian_filter(data, sigma=gaussian_sigma)
data = to_uint8(data, False)
mask = to_uint8(data, False) > masking * mh.otsu(to_uint8(data, False))

# Generate contour
contour = morphological_chan_vese(data, iterations=iterations,
                                  init_level_set=mask,
                                  smoothing=1, lambda1=1, lambda2=1)
contour = binary_extract_largest(contour)
# contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
contour = fill_beneath(contour)
contour = binary_extract_largest(contour)
contour = contour.astype('bool')

resolution = target_res
contour = np.pad(contour, 1) # just to make it so all tissue parts touching the image borders don't end up with big gaps
contour = contour[1:]

# Generate mesh
mesh = create_mesh(contour, resolution)
mesh = mesh.extract_largest()
mesh = mesh.clean()
mesh = repair_small(mesh, hole_repair_threshold)
mesh = remesh(mesh, int(mesh.n_points * downscaling), sub=0)
mm = mesh.copy()

mesh = remove_tongues(mesh, radius=tongues_radius, threshold=tongues_ratio, hole_edges=hole_repair_threshold)
mesh = mesh.extract_largest().clean()
mesh = repair_small(mesh, hole_repair_threshold)
mesh = mesh.smooth(smooth_iter, smooth_relax)
mesh = remesh(mesh, int(upscaling * mesh.n_points))
mesh = smooth_boundary(mesh, smooth_iter, smooth_relax)

mesh = mesh.extract_largest()
mesh = mesh.clean()
mesh = mesh.compute_normals()

mesh = mp.correct_normal_orientation_topcut(mesh, mesh.ray_trace([0] + list(mesh.center[1:]), mesh.center + np.array([9999999, 0, 0]))[0][0] - np.array([5, 0, 0]))
neighs = mp.vertex_neighbors_all(mesh)

# Calculate curvature
clim = curvature_limit   
mesh['curvature'] = -mesh.curvature('mean')
mesh['curvature'][mesh['curvature'] < -clim] = -clim
mesh['curvature'][mesh['curvature'] > clim] = clim
mesh['curvature'] = boa.minmax(mesh['curvature'], neighs, 20)
mesh['curvature'] = boa.set_boundary_values(mesh, scalars=mesh['curvature'], values=np.min(mesh['curvature']))
mesh['curvature'] = boa.mean(mesh['curvature'], neighs, 40) 

# mesh.plot(scalars='curvature', notebook=False, cmap='turbo', interpolate_before_map=False)

# Segment
mesh['domains'] = boa.steepest_ascent(mesh, scalars=mesh['curvature'], 
                                      neighbours=neighs, verbose=True)
 
mesh['domains'] = boa.merge_depth(mesh, domains=mesh['domains'],
      scalars=mesh['curvature'], threshold=0.0005, verbose=True, neighbours=neighs, mode='max')

# mesh['domains'] = boa.merge_border_length(mesh, mesh['domains'], threshold=75, neighbours=neighs, verbose=True)

mesh['domains'] = boa.merge_engulfing(mesh, domains=mesh['domains'], threshold=0.6,
      neighbours=neighs, verbose=True)

meristem_index = boa.define_meristem(mesh, mesh['domains'], method='com', neighs=neighs)

mesh['domains'] = boa.merge_disconnected(mesh, domains=mesh['domains'],
  meristem_index=meristem_index, threshold=20, neighbours=neighs, verbose=True)
meristem_index = boa.define_meristem(mesh, mesh['domains'], method='center_of_mass')

mesh['domains'] = boa.merge_angles(mesh, domains=mesh['domains'], meristem_index=meristem_index, threshold=12)
mesh.plot(scalars='domains', notebook=False, categories=True, cmap='glasbey', interpolate_before_map=False)


### PLOT
dd = tiff.imread(fname)
dd = to_uint8(data, False)
resolution = get_resolution(fname)
d = dd.copy()
d[d<50] = 0
cmap = 'viridis'
p = pv.Plotter(off_screen=True)
p.add_volume(d, cmap=cmap, opacity='sigmoid_6', show_scalar_bar=False, shade=True, diffuse=.9, resolution=resolution)
p.view_yz()
p.set_background('white')
# p.show()
p.screenshot(f'{OUTPUT_DIR}/example_injured-1_raw.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

contour = np.pad(contour, 1)
p = pv.Plotter(off_screen=True)
p.add_volume(contour.astype(float), cmap='tab20', opacity=[0] + [1] * 254, show_scalar_bar=False, shade=True, diffuse=.9, resolution=(.5, .5, .5))
p.view_yz()
p.set_background('white')
p.screenshot(f'{OUTPUT_DIR}/protocol-2_contour.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

p = pv.Plotter(off_screen=True)
# p = pv.Plotter(off_screen=False)
p.add_mesh(mm, scalars=None, cmap='tab20', show_scalar_bar=False, smooth_shading=True, show_edges=True, color='orange')
p.view_yz()
p.set_background('white')
# p.show()
p.screenshot(f'{OUTPUT_DIR}/protocol-3_mesh.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

p = pv.Plotter(off_screen=True)
# p = pv.Plotter(off_screen=False)
p.add_mesh(mesh, scalars='curvature', cmap='turbo', show_scalar_bar=False, smooth_shading=True, show_edges=False)
p.view_yz()
p.set_background('white')
# p.show()
p.screenshot(f'{OUTPUT_DIR}/protocol-4_curvature.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()

p = pv.Plotter(off_screen=True)
# p = pv.Plotter(off_screen=False)
p.add_mesh(mesh, scalars='domains', cmap='tab10', show_scalar_bar=False, smooth_shading=True, show_edges=False, interpolate_before_map=False)
p.view_yz()
p.set_background('white')
# p.show()
p.screenshot(f'{OUTPUT_DIR}/example_injured-2_segmentation.png', 
              transparent_background=True, window_size=[2000, 2000])
p.close()
p.deep_clean()
